const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const htmlPlugin = new HtmlWebPackPlugin({
    template: "./app/index.html",
    filename: "./index.html"
});

const webpackConfig = {
    devtool: 'source-map',

    entry: {
        // 'js/admin': path.resolve(__dirname, 'app/admin.js'),
        'js/shortcode': path.resolve(__dirname, 'app/shortcode.js'),
        // 'js/widget': path.resolve(__dirname, 'app/widget.js'),
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'assets'),
    },

    resolve: {
        extensions: [".js", ".jsx", ".json"],
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "css-loader" // translates CSS into CommonJS
                    }
                ]
            }
        ],
    },
    plugins: [htmlPlugin]
};

if (process.env.NODE_ENV === 'production') {
    webpackConfig.devtool = 'cheap-source-map';
}

module.exports = webpackConfig;
