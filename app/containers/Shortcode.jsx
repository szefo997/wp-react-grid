/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactTable from "react-table";
import axios from 'axios';
import xml2js from 'xml2js';
import {COLUMNS} from "../models/Columns";

export default class Shortcode extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            expanded: {},
            regions: [
                {v: null, t: ''},
                {v: 1, t: 'dolnośląskie'},
                {v: 2, t: 'kujawsko-pomorskie'},
                {v: 3, t: 'lubelskie'},
                {v: 4, t: 'lubuskie'},
                {v: 5, t: 'łódzkie'},
                {v: 6, t: 'małopolskie'},
                {v: 7, t: 'mazowieckie'},
                {v: 8, t: 'opolskie'},
                {v: 9, t: 'podkarpackie'},
                {v: 10, t: 'podlaskie'},
                {v: 11, t: 'pomorskie'},
                {v: 12, t: 'śląskie'},
                {v: 13, t: 'świętokrzyskie'},
                {v: 14, t: 'warmińsko-mazurskie'},
                {v: 15, t: 'wielkopolskie'},
                {v: 16, t: 'zachodniopomorskie'},
                {v: 17, t: 'zagranica'}
            ],
            work_type: [
                {v: null, t: ''},
                {v: 1, t: 'umowa o pracę'},
                {v: 2, t: 'umowa zlecenie'},
                {v: 3, t: 'B2B'},
                {v: 4, t: 'praca tymczasowa'},
                {v: 5, t: 'pełen etat'},
                {v: 6, t: 'cześć etatu'},
                {v: 7, t: 'dorywcza'},
                {v: 8, t: 'dla studenta'},
                {v: 9, t: 'dla niepełnosprawnych'},
                {v: 10, t: 'praca zdalna'},
                {v: 11, t: 'kadra zarządzająca'}
            ],
            categories: [
                {v: 1, t: 'Wszystkie branże'},
                {v: 2, t: 'Administracja biurowa'},
                {v: 3, t: 'Budownictwo'},
                {v: 4, t: 'BHP i ochrona środowiska'},
                {v: 5, t: 'Edukacja i szkolenia '},
                {v: 6, t: 'Finanse i księgowość'},
                {v: 7, t: 'Gastronomia'},
                {v: 8, t: 'HR'},
                {v: 9, t: 'IT'},
                {v: 10, t: 'Logistyka'},
                {v: 11, t: 'Marketing i reklama'},
                {v: 12, t: 'Motoryzacja'},
                {v: 13, t: 'Obsługa klienta'},
                {v: 14, t: 'Ochrona osób i mienia'},
                {v: 15, t: 'Pomoc domowa i opieka'},
                {v: 16, t: 'Praca fizyczna'},
                {v: 17, t: 'Produkcja i magazynowanie'},
                {v: 18, t: 'Prawo'},
                {v: 19, t: 'Projekty unijne'},
                {v: 20, t: 'Sprzedaż'},
                {v: 21, t: 'Telemarketing'},
                {v: 22, t: 'Transport'},
                {v: 23, t: 'Turystyka i hotelarstwo'},
                {v: 24, t: 'Zdrowie i uroda'},
                {v: 25, t: 'Inne'}
            ],
            data: [],
            columns: []
        };

        setTimeout(() => {
            this._initData();
        }, 50);
    }

    static onFilterContain(filter, row) {
        if (row[filter.id] === undefined) return false;
        return row[filter.id].toLowerCase().includes(filter.value.toLowerCase());
    }

    _initData() {
        const url = this._setUrl(url);
        const parser = new xml2js.Parser({
            explicitArray: false
        });

        axios.get(url)
            .then(response => {
                console.info('props', this.props);

                parser.parseString(response.data, (err, result) => {

                    this.setState(state => {
                        state.loading = false;
                        state.data = result['ADS']['AD'];
                        state.columns = [{
                            Header: 'Stanowisko',
                            accessor: COLUMNS.JOB_TITLE,
                            filterMethod: (filter, row) => {
                                if (row[filter.id] === undefined) return false;
                                return row._original[COLUMNS.FULL_HTML].toLowerCase().includes(filter.value.toLowerCase());
                            },
                            Filter: ({filter, onChange}) =>
                                <span className={'position'}><input type={'text'}
                                                                    placeholder={'słowo kluczowe…'}
                                                                    onChange={event => onChange(event.target.value)}
                                                                    style={{width: "100%"}}
                                >
                                </input><span className={'ico-mglass'}></span></span>,
                            minWidth: 150
                        }, {
                            Header: 'Województwo',
                            accessor: COLUMNS.REGION_ID,
                            Cell: ({value}) => this._mapToRegionType(value),
                            filterMethod: (filter, row) => row[filter.id] === filter.value,
                            Filter: ({filter, onChange}) =>
                                <select
                                    onChange={event => onChange(event.target.value)}
                                    style={{width: "100%"}}
                                >
                                    {state.regions.map((v, k) => {
                                        return <option key={k} value={v.v}>{v.t}</option>;
                                    })}
                                </select>,
                            minWidth: 150
                        }, {
                            Header: 'Branża',
                            accessor: COLUMNS.KATEGORIA,
                            Cell: ({value}) => this._mapToCategory(value),
                            filterMethod: (filter, row) => {
                                if (filter.value === "1")
                                    return true;
                                return row[filter.id] === filter.value;
                            },
                            Filter: ({filter, onChange}) =>
                                <select
                                    onChange={event => onChange(event.target.value)}
                                    style={{width: "100%"}}
                                >
                                    {state.categories.map((v, k) => {
                                        return <option key={k} value={v.v}>{v.t}</option>;
                                    })}
                                </select>,
                            minWidth: 150
                        }, {
                            Header: 'Rodzaj pracy',
                            accessor: COLUMNS.RODZAJ_PRACY,
                            Cell: ({value}) => this._mapToWorkType(value),
                            filterMethod: (filter, row) => {
                                const ids = row[filter.id].split(',');
                                for (const id of ids)
                                    if (Number(id) === Number(filter.value))
                                        return true;
                                return false;
                            },
                            Filter: ({filter, onChange}) =>
                                <select
                                    onChange={event => onChange(event.target.value)}
                                    style={{width: "100%"}}
                                >
                                    {state.work_type.map((v, k) => {
                                        return <option key={k} value={v.v}>{v.t}</option>;
                                    })}
                                </select>,
                            minWidth: 150
                        }];
                    });
                    this.forceUpdate();
                });
            });
    }

    _setUrl() {
        let url = "https://webservice.hrlink.pl/sync/api/grupaprofesjacom/export.php";
        if (this.props.wpObject !== undefined && this.props.wpObject.url)
            url = this.props.wpObject.url;
        return url;
    }

    render() {
        return (
            <div>
                <h2 className={'offer-title'}>Znajdź aktualną ofertę pracy</h2>
                <ReactTable
                    getTdProps={(state, rowInfo, column, instance) => {
                        return {
                            onClick: (e, handleOriginal) => {
                                let expanded = this.state.expanded;
                                expanded[rowInfo.index] = expanded[rowInfo.index] !== undefined ? !this.state.expanded[rowInfo.index] : true;
                                this.setState({
                                    expanded
                                });
                                if (handleOriginal) handleOriginal();
                            }
                        };
                    }}
                    expanded={this.state.expanded}
                    data={this.state.data}
                    previousText="Poprzednia"
                    nextText="Następna"
                    loadingText="Ładowanie"
                    noDataText="Brak danych"
                    pageText="Strona"
                    ofText="z"
                    loading={this.state.loading}
                    rowsText="wierszy"
                    showPageSizeOptions={false}
                    columns={this.state.columns}
                    filterable
                    defaultFilterMethod={Shortcode.onFilterContain}
                    SubComponent={row => this._renderSub(row)}
                />
            </div>
        );
    }

    _renderSub(row) {
        return <div className={'info-work'}>
            <div dangerouslySetInnerHTML={{
                __html: row.original[COLUMNS.FULL_HTML]
            }}/>
        </div>;
    }

    _mapToCategory(v) {
        for (const cat of this.state.categories)
            if (cat.v === Number(v))
                return cat.t;
    }

    _mapToWorkType(v) {
        const ids = v.split(',');
        const crafts = [];
        for (const wt of this.state.work_type) {
            for (const id of ids) {
                if (wt.v === Number(id))
                    crafts.push(wt.t);
            }
        }
        return crafts.join(', ');
    }

    _mapToRegionType(v) {
        for (const wt of this.state.regions)
            if (wt.v === Number(v))
                return wt.t;
    }

}

Shortcode.propTypes = {
    wpObject: PropTypes.object
};